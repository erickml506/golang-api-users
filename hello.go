package main

import (
	"encoding/json"
	"net/http"
)

type User struct {
	Id int `json:"id"`
	FirstName string `json:"firstName"`
	LastName string `json:"lastName"`
	Age int `json:"age"`
}


type Users struct {
	data []User
}

func main() {
	http.HandleFunc("/", foo)
	http.ListenAndServe(":1718", nil)
}


func foo(w http.ResponseWriter, r *http.Request) {
	data := make(map[string]User)

	data["1"] = User{1, "erick", "mendoza", 25}
	data["2"] = User{2, "erick1", "mendoza1", 21}
	data["3"] = User{3, "erick2", "mendoza2", 22}
	data["4"] = User{4, "erick3", "mendoza3", 24}

	js, err := json.Marshal(data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}
